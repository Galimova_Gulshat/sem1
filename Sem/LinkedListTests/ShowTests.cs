﻿using Sem;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LinkedListTests
{
    [TestClass]
    public class ShowTests
    {
        GraphicPic list = new GraphicPic(@"C:\Users\Gulshat\source\repos\filename.txt");

        [TestMethod]
        public void ToText()
        {
            var index = 0;
            var stringArray = list.ToString().Split(' ');
            foreach (var figure in list)
            {
                index += 6;
            }
            Assert.AreEqual(index, stringArray.Length - 1);
        }
    }
}
