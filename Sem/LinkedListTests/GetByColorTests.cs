﻿using Sem;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LinkedListTests
{
    [TestClass]
    public class GetByColorTests
    {
        GraphicPic list = new GraphicPic(@"C:\Users\Gulshat\source\repos\filename.txt");

        [TestMethod]
        public void GetByColor()
        {
            var newList = list.GetByColor(new Color[] { Color.Blue });
            var areTrue = true;
            foreach (var figure in newList)
            {
                if (figure.Hit != Color.Blue) areTrue = false;
            }
            Assert.AreEqual(true, areTrue);
        }

        [TestMethod]
        public void GetBySeveralColor()
        {
            var newList = list.GetByColor(new Color[] { Color.Blue, Color.Green });
            var areTrue = true;
            foreach (var figure in newList)
            {
                if (figure.Hit != Color.Blue && figure.Hit != Color.Green) areTrue = false;
            }
            Assert.AreEqual(true, areTrue);
        }

        [TestMethod]
        public void GetByAllColor()
        {
            var newList = list.GetByColor(new Color[] { Color.Blue, Color.Green, Color.Ocher, Color.Orange, Color.Red });
            Assert.AreEqual(list.Count, newList.Count);
        }

        [TestMethod]
        public void GetByNoColor()
        {
            var newList = list.GetByColor(new Color[] { });
            Assert.AreEqual(0, newList.Count);
        }

    }
}
