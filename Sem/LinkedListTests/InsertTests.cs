﻿using System;
using Sem;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LinkedListTests
{
    [TestClass]
    public class InsertTests
    {
        GraphicPic list = new GraphicPic(@"C:\Users\Gulshat\source\repos\filename.txt");
        Figure figure = new Figure() { Types = Sem.Type.Circle, Hit = Color.Blue, LX = 3, LY = 3, RX = 5, RY = 0 };

        [TestMethod]
        public void Insert()
        {
            list.Insert(figure);
            Assert.AreEqual(8, list.Count);
            Assert.AreEqual(figure, list.Last.Value);
        }

        [TestMethod]
        public void InsertInEmptyList()
        {
            var emptyList = new GraphicPic();
            emptyList.Insert(figure);
            Assert.AreEqual(1, emptyList.Count);
            Assert.AreEqual(figure, emptyList.First.Value);
            Assert.AreEqual(figure, emptyList.Last.Value);
        }

        [TestMethod]
        public void InsertNoSimilar()
        {
            list.Insert(new Figure() { Types = Sem.Type.Circle, Hit = Color.Green, LX = 3, LY = 3, RX = 6, RY = 0 });
            list.Insert(figure);
            Assert.AreEqual(9, list.Count);
            Assert.AreEqual(figure, list.Last.Value);
        }

        [TestMethod]
        public void InsertSimilar()
        {
            list.Insert(new Figure() { Types = Sem.Type.Circle, Hit = Color.Green, LX = 3, LY = 3, RX = 5, RY = 0 });
            list.Insert(figure);
            Assert.AreEqual(8, list.Count);
            Assert.AreEqual(figure, list.Last.Value);
        }

        [TestMethod]
        public void InsertNull()
        {
            Exception exception = new Exception();
            try
            {
                list.Insert((new Figure()));
            }
            catch (ArgumentException e)
            {
                exception = e;
            }
            Assert.IsNotNull(exception);
        }
    }
}
