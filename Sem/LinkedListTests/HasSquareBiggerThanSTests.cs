﻿using System;
using Sem;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LinkedListTests
{
    [TestClass]
    public class HasSquareBiggerThanSTests
    {
        GraphicPic list = new GraphicPic(@"C:\Users\Gulshat\source\repos\filename.txt");

        [TestMethod]
        public void HasSquareBiggerThanS()
        {
            var newList = list.HasSquareBiggerThanS(4);
            double seg = 0;
            var areTrue = true;
            foreach (var figure in newList)
            {
                if (figure.Types == Sem.Type.Rectangle) seg = (figure.RX - figure.LX) * (figure.RY - figure.LY);
                else if (figure.Types == Sem.Type.Segment) seg = 0;
                else if (figure.Types == Sem.Type.Circle) seg = Math.PI * figure.RX * figure.RX;
                if (seg <= 4) areTrue = false;
            }
            Assert.AreEqual(true, areTrue);
            Assert.AreEqual(4, newList.Count);
        }

        [TestMethod]
        public void HasSquareBiggerThanS1()
        {
            var newList = list.HasSquareBiggerThanS(6);
            double seg = 0;
            var areTrue = true;
            foreach (var figure in newList)
            {
                if (figure.Types == Sem.Type.Rectangle) seg = (figure.RX - figure.LX) * (figure.RY - figure.LY);
                else if (figure.Types == Sem.Type.Segment) seg = 0;
                else if (figure.Types == Sem.Type.Circle) seg = Math.PI * figure.RX * figure.RX;
                if (seg <= 6) areTrue = false;
            }
            Assert.AreEqual(true, areTrue);
            Assert.AreEqual(3, newList.Count);
        }

        [TestMethod]
        public void HasSquareBiggerThanNegative()
        {
            Exception exception = new Exception();
            try
            {
                var newList = list.HasSquareBiggerThanS(-1);
            }
            catch (ArgumentException e)
            {
                exception = e;
            }
            Assert.IsNotNull(exception);
        }

        [TestMethod]
        public void HasSquareBiggerThanZero()
        {
            var newList = list.HasSquareBiggerThanS(0);
            var index = 0;
            foreach (var figure in newList)
            {
                if (figure.Types != Sem.Type.Segment) index++;
            }
            Assert.AreEqual(index, newList.Count);
        }
    }
}
