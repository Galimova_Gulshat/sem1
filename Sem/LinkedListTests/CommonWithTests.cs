﻿using System;
using Sem;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LinkedListTests
{
    [TestClass]
    public class CommonWithTests
    {
        GraphicPic list = new GraphicPic(@"C:\Users\Gulshat\source\repos\Sem\filename.txt");

        [TestMethod]
        public void CommonWithAll()
        {
            var newList = list.CommonWith((new Figure() { Types = Sem.Type.Rectangle, Hit = Color.Blue, LX = 4, LY = 12, RX = 12, RY = 2 }));
            Assert.AreEqual(list.Count, newList.Count);
        }
        

        [TestMethod]
        public void NotCommonWith()
        {
            var newList = list.CommonWith((new Figure() { Types = Sem.Type.Rectangle, Hit = Color.Blue, LX = 6, LY = 1, RX = 7, RY = 0 }));
            Assert.AreEqual(0, newList.Count);
            Assert.AreEqual(null, newList.First);
            Assert.AreEqual(null, newList.Last);
        }

        [TestMethod]
        public void CommonWith()
        {
            var newList = list.CommonWith((new Figure() { Types = Sem.Type.Rectangle, Hit = Color.Blue, LX = 4, LY = 12, RX = 9, RY = 11 }));
            Assert.AreEqual(3, 3);
        }

        [TestMethod]
        public void CommonWithNull()
        {
            Exception exception = new Exception();
            try
            {
                var newList = list.CommonWith((new Figure()));
            }
            catch (ArgumentException e)
            {
                exception = e;
            }
            Assert.IsNotNull(exception);
        }

        [TestMethod]
        public void CommonWithCircle()
        {
            Exception exception = new Exception();
            try
            {
                var newList = list.CommonWith((new Figure() { Types = Sem.Type.Circle, Hit = Color.Blue, LX = 6, LY = 5, RX = 9, RY = 4}));
            }
            catch (ArgumentException e)
            {
                exception = e;
            }
            Assert.IsNotNull(exception);
        }

        [TestMethod]
        public void CommonWithSegment()
        {
            var newList = list.CommonWith((new Figure() { Types = Sem.Type.Rectangle, Hit = Color.Blue, LX = 3, LY = 14, RX = 3, RY = 2 }));
            Assert.AreEqual(4, newList.Count);
        }
    }
}
