﻿using System;
using Sem;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace LinkedListTests
{
    [TestClass]
    public class UnitTest1
    {
        GraphicPic list = new GraphicPic("filename.txt");
        Figure figure = new Figure() { Types = Sem.Type.Circle, Colors = Color.Blue, LX = 3, LY = 3, RX = 5, RY = 0 };

        [TestMethod]
        public void Insert()
        {
            list.Insert(figure);
            Assert.AreEqual(8, list.Count);
            Assert.AreEqual(figure, list.last.Value);
        }

        [TestMethod]
        public void InsertInEmptyList()
        {
            var emptyList = new GraphicPic();
            emptyList.Insert(figure);
            Assert.AreEqual(1, emptyList.Count);
            Assert.AreEqual(figure, emptyList.first.Value);
            Assert.AreEqual(figure, emptyList.last.Value);
        }

        [TestMethod]
        public void InsertSimilar()
        {
            list.Insert(new Figure() { Types = Sem.Type.Circle, Colors = Color.Green, LX = 3, LY = 3, RX = 5, RY = 0 });
            list.Insert(figure);
            Assert.AreEqual(8, list.Count);
            Assert.AreEqual(figure, list.last.Value);
        }

        /*[TestMethod]
        public void Show()
        {
            list.Show();
            var lines = Console.ReadLine();
            var figures = File.ReadAllText("filename.txt");
            Assert.AreEqual(lines, figures);
        }*/

        /*[TestMethod]
        public void ShowAfterInsert()
        {
            list.Insert(figure);
            list.Show();
            var lines = Console.ReadLine();
            var figures = File.ReadAllText("filename.txt");
            Assert.AreEqual(lines, figures);
        }*/

        [TestMethod]
        public void Delete()
        {
            var areTrue = true;
            list.Delete(Sem.Type.Circle);
            for(GraphicPicNode i = list.first; i != null; i = i.Next)
            {
                if (i.Value.Types == Sem.Type.Circle) areTrue = false;
            }
            Assert.AreEqual(true, areTrue);
        }

        [TestMethod]
        public void DeleteCircle()
        {
            var areTrue = true;
            list.Delete(Sem.Type.Circle);
            for (GraphicPicNode i = list.first; i != null; i = i.Next)
            {
                if (i.Value.Types == Sem.Type.Circle) areTrue = false;
            }
            Assert.AreEqual(true, areTrue);
        }

        [TestMethod]
        public void DeleteRectangle()
        {
            var areTrue = true;
            list.Delete(Sem.Type.Rectangle);
            for (GraphicPicNode i = list.first; i != null; i = i.Next)
            {
                if (i.Value.Types == Sem.Type.Rectangle) areTrue = false;
            }
            Assert.AreEqual(true, areTrue);
        }

        [TestMethod]
        public void DeleteSegment()
        {
            var areTrue = true;
            list.Delete(Sem.Type.Segment);
            for (GraphicPicNode i = list.first; i != null; i = i.Next)
            {
                if (i.Value.Types == Sem.Type.Segment) areTrue = false;
            }
            Assert.AreEqual(true, areTrue);
        }

        [TestMethod]
        public void CommonWith()
        {
            var newlist = list.CommonWith((new Figure() { Types = Sem.Type.Rectangle, Colors = Color.Blue, LX = 4, LY = 2, RX = 12, RY = 11 }));
            Assert.AreEqual(list.Count, newlist.Count);
        }

        [TestMethod]
        public void CommonWithAll()
        {
            var newlist = list.CommonWith((new Figure() { Types = Sem.Type.Rectangle, Colors = Color.Blue, LX = 3, LY = 9, RX = 9, RY = 12 }));
            Assert.AreEqual(2, newlist.Count);
        }

        [TestMethod]
        public void NotCommonWith()
        {
            var newlist = list.CommonWith((new Figure() { Types = Sem.Type.Rectangle, Colors = Color.Blue, LX = 6, LY = 0, RX = 7, RY = 1 }));
            Assert.AreEqual(0, newlist.Count);
            Assert.AreEqual(null, newlist.first);
            Assert.AreEqual(null, newlist.last);
        }

        [TestMethod]
        public void HasSquareBiggerThanS()
        {
            var newlist = list.HasSquareBiggerThanS(4);
            double seg = 0;
            var areTrue = true;
            for (GraphicPicNode i = newlist.first; i?.Next != null; i = i.Next)
            {
                var j = i.Value;
                if (j.Types == Sem.Type.Rectangle) seg = (j.RX - j.LX) * (j.RY - j.LY);
                else if (j.Types == Sem.Type.Segment) seg = 0;
                else if (j.Types == Sem.Type.Circle) seg = Math.PI * j.RX * j.RX;
                if (seg <= 4) areTrue = false;
            }
            Assert.AreEqual(true, areTrue);
            Assert.AreEqual(4, newlist.Count);
        }

        [TestMethod]
        public void HasSquareBiggerThanNegative()
        {
            var newlist = list.HasSquareBiggerThanS(-1);
            Assert.AreEqual(list.Count, newlist.Count);
        }

        [TestMethod]
        public void HasSquareBiggerThanZero()
        {
            var newlist = list.HasSquareBiggerThanS(0);
            var index = 0;
            for (GraphicPicNode i = list.first; i != null; i = i.Next)
            {
                if (i.Value.Types != Sem.Type.Segment) index++;
            }
            Assert.AreEqual(index, newlist.Count);
        }

        [TestMethod]
        public void GetByColor()
        {
            var newlist = list.GetByColor(new Color[] { Color.Blue });
            var areTrue = true;
            for (GraphicPicNode i = newlist.first; i != null; i = i.Next)
            {
                if (i.Value.Colors != Color.Blue) areTrue = false;
            }
            Assert.AreEqual(true, areTrue);
        }

        [TestMethod]
        public void GetByAllColor()
        {
            var newlist = list.GetByColor(new Color[] { Color.Blue, Color.Green, Color.Ocher, Color.Orange, Color.Red });
            Assert.AreEqual(list.Count, newlist.Count);
        }

    }
}
