﻿using System;
using Sem;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LinkedListTests
{
    [TestClass]
    public class DeleteTests
    {
        GraphicPic list = new GraphicPic(@"C:\Users\Gulshat\source\repos\filename.txt");
        Figure figure = new Figure() { Types = Sem.Type.Circle, Hit = Color.Blue, LX = 3, LY = 3, RX = 5, RY = 0 };

        [TestMethod]
        public void DeleteCircle()
        {
            var areTrue = true;
            list.Delete(Sem.Type.Circle);
            foreach (var figure in list)
            {
                if (figure.Types == Sem.Type.Circle) areTrue = false;
            }
            Assert.AreEqual(true, areTrue);
        }

        [TestMethod]
        public void DeleteRectangle()
        {
            var areTrue = true;
            list.Delete(Sem.Type.Rectangle);
            foreach (var figure in list)
            {
                if (figure.Types == Sem.Type.Rectangle) areTrue = false;
            }
            Assert.AreEqual(true, areTrue);
        }

        [TestMethod]
        public void DeleteSegment()
        {
            var areTrue = true;
            list.Delete(Sem.Type.Segment);
            foreach (var figure in list)
            {
                if (figure.Types == Sem.Type.Segment) areTrue = false;
            }
            Assert.AreEqual(true, areTrue);
        }

        [TestMethod]
        public void DeleteFromEmptyList()
        {
            var newList = new GraphicPic();
            Exception exception = null;
            try
            {
                newList.Delete(Sem.Type.Segment);
            }
            catch (ArgumentNullException e)
            {
                exception = e;
            }
            Assert.IsNotNull(exception);
        }

        [TestMethod]
        public void DeleteNoSegment()
        {
            var areTrue = true;
            var newList = new GraphicPic();
            newList.Insert(figure);
            newList.Delete(Sem.Type.Segment);
            foreach (var figure in newList)
            {
                if (figure.Types == Sem.Type.Segment) areTrue = false;
            }
            Assert.AreEqual(true, areTrue);
            Assert.AreEqual(1, newList.Count);
        }
    }
}
