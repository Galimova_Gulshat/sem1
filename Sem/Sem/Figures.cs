﻿

namespace Sem
{
    public enum Type { Rectangle, Segment, Circle };
    public enum Color { Red, Green, Blue, Orange, Ocher };

    public class Figure
    {
        public Type Types { get; set; }
        public int LX { get; set; }
        public int LY { get; set; }
        public int RX { get; set; }
        public int RY { get; set; }
        public Color Hit { get; set; }
    }
}
