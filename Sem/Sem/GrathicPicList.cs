﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Sem
{
    public class GraphicPic : IEnumerable<Figure>
    {
        public class GraphicPicNode
        {
            public Figure Value { get; set; }
            public GraphicPicNode Next { get; set; }
        }

        public GraphicPicNode First { get; private set; }
        public GraphicPicNode Last { get; private set; }
        public int Count { get; private set; }

        public GraphicPic()
        {
        }

        public GraphicPic(String filename)
        {
            //построение списка по множеству фигур, заданному в текстовом файле;
            var line = File.ReadAllText(filename).Split(' ');
            Figure figure = new Figure();
            for(int i = 0; i < line.Length; i++)
            {
                switch (line[i])
                {
                    case "Rectangle":
                    case "Segment":
                    case "Circle":
                        Add(CreateFigure(line, i));
                        i += 5;
                        break;
                }
            }          
        }

        private Figure CreateFigure(string[] line, int i)
        {
            var figure = new Figure()
            {
                Types = (Type)Enum.Parse(typeof(Type), line[i]),
                LX = Convert.ToInt32(line[i + 1]),
                LY = Convert.ToInt32(line[i + 2]),
                RX = Convert.ToInt32(line[i + 3]),
                RY = Convert.ToInt32(line[i + 4]),
                Hit = (Color)Enum.Parse(typeof(Color), line[i + 5])
            };
            if (figure.Types == Type.Circle) figure.RY = 0;
            return figure;
        }

        public void Show()
        {
            //вывод всех фигур и информации о них на экран; 
            Console.WriteLine(ToString());
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            Figure figure;
            for (GraphicPicNode i = First; i != null; i = i.Next)
            {
                figure = i.Value;
                builder.Append($"{figure.Types} {figure.LX} {figure.LY} {figure.RX} {figure.RY} {figure.Hit} \n");
            }
            return builder.ToString();
        }

        public void Insert(Figure f)
        {
            //вставка фигуры в список. При вставке учесть, существует
            //ли подобный элемент в списке, если да – то заменить существующий, например, может поменяться цвет фигуры; 
            if (f == null) throw new ArgumentException();
            var node = new GraphicPicNode { Value = f, Next = null };

            if (First == null)
            {
                First = Last = node;
            }
            else
            {
                for (GraphicPicNode i = First; i != null; i = i.Next)
                {
                    if (i.Value.Types == node.Value.Types && AreSimilar(i.Value, node.Value))
                    {
                        i.Value = node.Value;
                        return;
                    }
                }
                Last.Next = node;
                Last = node;
            }
            Count++;

        }

        private bool AreSimilar(Figure i, Figure node)
        {
            var index = 0;
            if (i.LX == node.LX)
                index++;
            if (i.LY == node.LY)
                index++;
            if (i.RX == node.RX)
                index++;
            if (i.RY == node.RY && i.Types != Type.Circle)
                index++;
            if (i.Hit == node.Hit)
                index++;
            if(i.Types == Type.Circle) return index >= 3;
            return index >= 4;
        }

        public void Delete(Type i)
        {
            //удалить из списка фигуры с типом i. 
            if (Count == 0) throw new ArgumentNullException();
            var previous = First;
            if (First.Value.Types == i)
            {
                First = First.Next;
            }
            if (First == null) Last = null;
            for (GraphicPicNode j = First; j != null; j = j.Next)
            {
                if(j.Value.Types == i)
                {
                    previous.Next = j.Next;
                    if (j.Next == null) Last = previous;
                    Count--;
                }
                else previous = j;
            }
        }

        public GraphicPic CommonWith(Figure r)
        {
            //построить новый список, 
            //состоящий из фигур, которые имеют общие точки с некоторым прямоугольником. Исходный список не трогать. 
            if (r == null || r.Types != Type.Rectangle) throw new ArgumentException();
            var commonGraphicPic = new GraphicPic();
            var left = true;
            var top = true;
            for (GraphicPicNode i = First; i != null; i = i.Next)
            {
                var j = i.Value;
                if(j.Types == Type.Rectangle)
                {
                    left = AreIntersectedLine(r.LX, r.RX, j.LX,  j.RX);
                    top = AreIntersectedLine(r.RY, r.LY, j.RY, j.LY);
                    if (left && top) commonGraphicPic.Add(j);
                }
                else if (j.Types == Type.Segment)
                {
                    left = AreIntersectedLine(r.LX, r.RX, Math.Min(j.LX, j.RX), Math.Max(j.LX, j.RX));
                    top = AreIntersectedLine(r.RY, r.LY, Math.Min(j.LY, j.RY), Math.Max(j.LY, j.RY));
                    if (left && top) commonGraphicPic.Add(j);
                }
                
                else if (j.Types == Type.Circle)
                {
                    if (Intersect(r, j)) commonGraphicPic.Add(j);
                }
            }
            return commonGraphicPic;
        }

        private bool AreIntersectedLine(double left1, double right1, double left2, double right2)
        {
            return Math.Min(right1, right2) >= Math.Max(left1, left2);
        }

        private static bool Intersect(Figure r, Figure j)
        {
            var figure = new Figure() { Types = Type.Rectangle, LX = r.LX - j.LX, LY = r.LY - j.LY, RX = r.RX - j.LX, RY = r.RY - j.LY };

            var deltaX = r.LX - r.RX;
            var deltaY = r.LY - r.RY;

            var d1 = r.LX - j.LX;
            var d2 = r.RY - j.LX;

            double b = figure.LX * deltaX + figure.LY * deltaY;
            double d = figure.LX * figure.RX + figure.LY * figure.RY;
            double c = figure.LX * figure.LX + figure.LY * figure.LY - j.RX * j.RX;
            double c1 = d1 * d1 + d2 * d2 - j.RX * j.RX;

            if (b < 0) return c <= 0 || c1 <= 0 || d < 0;
            else return true;
        }

        

        public GraphicPic HasSquareBiggerThanS(double s) 
        {
            //построить новый список из фигур, площади которых более s. Исходный список не трогать
            if (s < 0) throw new ArgumentException("s can't be negative");
            var sGraphicPic = new GraphicPic();
            double square = 0;
            for (GraphicPicNode i = First; i != null; i = i.Next)
            {
                var j = i.Value;
                if (j.Types == Type.Rectangle) square = (j.RX - j.LX) * (j.RY - j.LY);
                else if (j.Types == Type.Segment) square = 0;
                else if (j.Types == Type.Circle) square = Math.PI * j.RX * j.RX;
                if (square > s) sGraphicPic.Add(j);
            }
            return sGraphicPic;
        }

        public GraphicPic GetByColor(Color[] colors)
        {
            //построить новый список, содержащий только фигуры цветов из массива colors.
            var colorGraphicPic = new GraphicPic();
            for (GraphicPicNode i = First; i != null; i = i.Next)
            {
                foreach(var color in colors)
                {
                    if(i.Value.Hit == color)
                    {
                        colorGraphicPic.Add(i.Value);
                        break;
                    }
                }
            }
            return colorGraphicPic;
        }

        public IEnumerator<Figure> GetEnumerator()
        {
            for (GraphicPicNode i = First; i != null; i = i.Next)
                yield return i.Value;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private void Add(Figure f)
        {
            var node = new GraphicPicNode { Value = f, Next = null };

            if (First == null)
            {
                First = Last = node;
            }
            else
            {
                Last.Next = node;
                Last = node;
            }
            Count++;
        }
    }
}
