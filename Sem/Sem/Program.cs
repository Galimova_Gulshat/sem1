﻿using System;

namespace Sem
{
    class Program
    {
        static void Main(string[] args)
        {
            var list = new GraphicPic("filename.txt");
            Write(list);

            Console.WriteLine($"Count = {list.Count}");
            Console.WriteLine();

            list.Insert(new Figure() { Types = Type.Rectangle, Hit = Color.Blue, LX = 9, LY = -1, RX = 12, RY = 1 });
            Write(list);

            var commonList = list.CommonWith(new Figure() { Types = Type.Rectangle, Hit = Color.Blue, LX = 4,
                LY = 2, RX = 12, RY = 11 });
            Write(commonList);

            var sList = list.HasSquareBiggerThanS(4);
            Write(sList);

            var colorList = list.GetByColor(new Color[] { Color.Red, Color.Blue });
            Write(colorList);

            list.Delete(Type.Circle);
            Write(list);
        }

        static void Write(GraphicPic graphic)
        {
            graphic.Show();
            Console.WriteLine();
        }
    }
}
